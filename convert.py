from git import *
from pprint import pprint

ROOT = "/home/eugeneai/tmp/linux"
#ROOT = "/home/eugeneai/Development/codes/dispersive"

repo = Repo(ROOT)
print "Is it bare?:", repo.bare
print "HEADS:", repo.heads
print "TAGS:", len(repo.tags)

f = open(ROOT+"/_.txt", 'w')
i=0
for comm in repo.iter_commits('master', max_count=100000):
	i+=1
	c = comm.hexsha
	f.write ('node (')
	f.write (str(c))
	f.write (')')
	f.write ('\n')
	#pprint (dir(comm))
	print i, comm.message.strip(), comm.committer

	break
f.close()
